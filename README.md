# Fimbles

Word game written in Java with the Spring framework.

Install inside resources/static/js:
npm install --global babel-cli
npm install babel-preset-react

To watch and compile Reactjs:
babel --presets react src --watch --out-dir build