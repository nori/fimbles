package project;

/**
 * Created by arnor on 11/22/15.
 */
public enum GameState {
    WRITE_EXPLANATION, VOTE_EXPLANATION, SHOW_RESULTS
}
