package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.persistence.Player;
import project.persistence.Slot;
import project.persistence.SlotRepository;

import java.util.List;
/**
 * Created by Runar on 26.10.2015.
 */
@Service
public class SlotService {
    SlotRepository repository;
    @Autowired
    public SlotService(SlotRepository repository){
        this.repository = repository;
    }
    public Slot save (Slot slot){
        return repository.save(slot);
    }
    public void delete(Slot slot) {
        repository.delete(slot);
    }
    public List<Slot> findAll(){
        return repository.findAll();
    }
    public Slot findOne(Long id){
        return repository.findOne(id);
    }
    public List<Slot> findByPlayer(Player player){
        return repository.findByPlayer(player);
    }
}
