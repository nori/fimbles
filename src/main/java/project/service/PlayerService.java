package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import project.form.PlayerCreateForm;
import project.persistence.Player;
import project.persistence.PlayerRepository;

import java.util.List;

/**
 * Created by Tomas on 26/10/2015.
 */
@Service
public class PlayerService {
    private final PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public Player getUserById(long id) {
        return playerRepository.findOne(id);
    }

    public Player getUserByEmail(String email) {
        return playerRepository.findOneByEmail(email);
    }

    public Player getUserByUsername(String username) {
        return playerRepository.findOneByUsername(username);
    }

    public List<Player> getAllUsers() {
        return playerRepository.findAll();
    }

    public Player create(PlayerCreateForm form) {
        Player player = new Player();
        player.setEmail(form.getEmail());
        player.setPassword(new BCryptPasswordEncoder().encode(form.getPassword()));
        player.setRole(form.getRole());
        player.setUsername(form.getUsername());
        return playerRepository.save(player);
    }

    public Player save (Player player){
        return this.playerRepository.save(player);
    }

    public void delete(Player player) {
        this.playerRepository.delete(player);
    }
}
