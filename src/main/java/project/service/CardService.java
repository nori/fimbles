package project.service;

/**
 * Created by ArnthorHelgi on 10/25/2015.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.persistence.Card;
import project.persistence.CardRepository;

import java.util.Collections;
import java.util.List;

@Service
public class CardService {
    CardRepository repository;
    @Autowired
    public CardService(CardRepository repository){
        this.repository = repository;
    }
    public Card save (Card card){
        return repository.save(card);
    }
    public void delete(Card card) {
        repository.delete(card);
    }
    public List<Card> findAll(){
        return repository.findAll();
    }
    public List<Card> findAllShuffled(){
        List<Card> cards = findAll();
        Collections.shuffle(cards);
        return cards;
    }
    public Card findOne(Long id){
        return repository.findOne(id);
    }
    public List<Card> findByWord(String word){
        return repository.findByWord(word);
    }

}
