package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import project.persistence.CurrentUser;
import project.persistence.Player;

/**
 * Created by Arnthor Helgi on 11/15/2015.
 */
@Service
public class CurrentPlayerDetailsService implements UserDetailsService {
    private final PlayerService playerService;

    @Autowired
    public CurrentPlayerDetailsService(PlayerService playerService){
        this.playerService = playerService;
    }
    public CurrentUser loadUserByUsername(String email) throws UsernameNotFoundException {
        Player user = playerService.getUserByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("User with email=%s was not found", email));
        }
        return new CurrentUser(user);
    }
}
