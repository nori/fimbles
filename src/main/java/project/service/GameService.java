package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.persistence.Game;
import project.persistence.GameRepository;
import project.persistence.Player;
import project.persistence.Slot;

import java.util.List;
import java.util.Optional;

/**
 * Created by Runar on 26.10.2015.
 */
@Service
public class GameService {
    GameRepository repository;
    @Autowired
    public GameService(GameRepository repository){
        this.repository = repository;
    }
    public Game save (Game game){
        return repository.save(game);
    }
    public void delete(Game game) {
        repository.delete(game);
    }
    public List<Game> findAll(){
        return repository.findAll();
    }
    public Game findOne(Long id){
        return repository.findOne(id);
    }
    public Long findGameByPlayer(Player player){
        for(Game g : findAll()){
            for(Slot s: g.getSlots()){
                if(s.getPlayer() == player){
                    return g.getId();
                }
            }
        }
        return -1L;
    }
    public boolean hasPlayer(long id, Player player) {
        Game game = findOne(id);
        for (Slot s : game.getSlots()) {
            if (s.getPlayer() == player) {
                return true;
            }
        }

        return false;
    }

    public Optional<Slot> getSlot(long id, Player player) {
        Game game = findOne(id);
        for (Slot s : game.getSlots()) {
            if (s.getPlayer() == player) {
                return Optional.of(s);
            }
        }

        return Optional.empty();
    }
}
