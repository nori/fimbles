package project.persistence;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Runar on 26.10.2015.
 */
@Entity
public class Slot implements Comparable<Slot> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Player player;

    private int points;
    private int votes;
    private String submission;
    private int vote;
    private Date lastPing;
    private Integer missedSubmissions;
    private int currentRoundPoints;

    public Slot(Player player) {
        this.player = player;
        vote = -1;
        missedSubmissions = 0;
        this.currentRoundPoints=0;
    }

    public int getCurrentRoundPoints() {
        return currentRoundPoints;
    }

    public void setCurrentRoundPoints(int currentRoundPoints) {
        this.currentRoundPoints = currentRoundPoints;
    }

    public Slot() {
        this(null);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getSubmission() {
        return submission;
    }

    public void setSubmission(String submission) {
        this.submission = submission;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }

    public Date getLastPing() {
        return lastPing;
    }

    public void setLastPing(Date lastPing) {
        this.lastPing = lastPing;
    }

    public int getMissedSubmissions() {
        if (missedSubmissions == null) {
            return 0;
        }
        return missedSubmissions;
    }

    public void setMissedSubmissions(int missedSubmissions) {
        this.missedSubmissions = missedSubmissions;
    }

    @Override
    public int compareTo(Slot o) {
        return Long.compare(id, o.getId());
    }
}
