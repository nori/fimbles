package project.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * Created by Runar on 26.10.2015.
 */
@Repository
public interface SlotRepository extends JpaRepository<Slot, Long> {
    List<Slot> findByPlayer(Player player);
}
