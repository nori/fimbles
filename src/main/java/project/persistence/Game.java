package project.persistence;

import project.GameState;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Runar on 26.10.2015.
 */
@Entity
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany
    private List<Slot> slots;
    private int currentRound;
    @ManyToMany
    private List<Card> deck;
    private Date stateTimeStamp;
    @Enumerated(EnumType.STRING)
    private GameState state;

    public Game() {
        currentRound = 1;
        state = GameState.WRITE_EXPLANATION;
        stateTimeStamp = new Date();
        slots = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public List<Card> getDeck() {
        return deck;
    }

    public void setDeck(List<Card> deck) {
        this.deck = deck;
    }

    public Date getStateTimeStamp() {
        return stateTimeStamp;
    }

    public void setStateTimeStamp(Date stateTimeStamp) {
        this.stateTimeStamp = stateTimeStamp;
    }

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
        setStateTimeStamp(new Date());
    }
}
