package project.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import project.Role;

/**
 * Created by arnor on 11/16/15.
 */
@Component
public class DatabaseLoader implements CommandLineRunner {
    private final PlayerRepository playerRepository;

    @Autowired
    public DatabaseLoader(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (playerRepository.count() == 0) {
            Player player = new Player();
            player.setEmail("admin@admin.com");
            player.setPassword(new BCryptPasswordEncoder().encode("admin"));
            player.setRole(Role.ADMIN);
            player.setUsername("admin");
            playerRepository.save(player);
        }
    }
}
