package project.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Tomas on 26/10/2015.
 */
@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {
    Player findOneByUsername(String username);
    Player findOneByEmail(String email);
}
