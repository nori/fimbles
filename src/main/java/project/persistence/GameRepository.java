package project.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Runar on 26.10.2015.
 */
@Repository
public interface GameRepository extends JpaRepository<Game, Long> {
}
