package project.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ArnthorHelgi on 10/25/2015.
 */
@Repository
public interface CardRepository extends JpaRepository<Card, Long> {
    List<Card> findByWord(String word);
}
