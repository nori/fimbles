package project.persistence;

import org.springframework.security.core.authority.AuthorityUtils;
import project.Role;

/**
 * Created by Arnthor Helgi on 11/15/2015.
 */
public class CurrentUser extends org.springframework.security.core.userdetails.User {
    private Player player;

    public CurrentUser(Player player){
        super(player.getEmail(), player.getPassword(), AuthorityUtils.createAuthorityList(player.getRole().toString()));
        this.player = player;
    }
    public Player getUser() {
        return player;
    }

    public Long getId() {
        return player.getId();
    }

    public Role getRole() {
        return player.getRole();
    }
}
