package project.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import project.Role;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Tomas on 26/10/2015.
 */
@Entity
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;
    private String username;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false, unique = true)
    private String email;
    private Date timeStamp;
    private int submissions;
    private int totalPoints;
    private boolean isBanned;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    public Player() {}

    @JsonIgnore
    public Role getRole() {
        return role;
    }

    @JsonIgnore
    public void setRole(Role role) {
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    public String getEmail() {
        return email;
    }

    @JsonIgnore
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonIgnore
    public void setPassword(String password) {
        this.password = password;
    }

    public Date getTimeStamp() {

        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getSubmissions() {
        return submissions;
    }

    public void setSubmissions(int submissions) {
        this.submissions = submissions;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    @JsonIgnore
    public boolean isBanned() {
        return isBanned;
    }

    @JsonIgnore
    public void setBanned(boolean isBanned) {
        this.isBanned = isBanned;
    }
}
