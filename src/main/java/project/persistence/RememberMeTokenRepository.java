package project.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by arnor on 11/16/15.
 */
@Repository
public interface RememberMeTokenRepository extends JpaRepository<RememberMeToken, Long> {
    RememberMeToken findBySeries(String series);
    List<RememberMeToken> findByUsername(String username);
}
