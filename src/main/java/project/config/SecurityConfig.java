package project.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import project.persistence.PersistentTokenRepositoryImpl;
import project.persistence.RememberMeTokenRepository;
import project.service.CurrentPlayerDetailsService;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CurrentPlayerDetailsService playerDetailsService;

    @Autowired
    private RememberMeTokenRepository rememberMeTokenRepository;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        PersistentTokenRepositoryImpl tokenRepository = new PersistentTokenRepositoryImpl(rememberMeTokenRepository);
        http.authorizeRequests()
                .antMatchers("/", "/public/**", "/signup", "/fimbles.apk").permitAll()
                .antMatchers("/player/**").hasAuthority("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .failureUrl("/login?error")
                .usernameParameter("email")
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                .deleteCookies("remember-me")
                .logoutSuccessUrl("/")
                .permitAll()
                .and()
                .rememberMe().tokenRepository(tokenRepository);
        http.httpBasic();
        http.csrf().disable();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(playerDetailsService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }

}