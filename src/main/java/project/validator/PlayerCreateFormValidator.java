package project.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import project.form.PlayerCreateForm;
import project.service.PlayerService;


/**
 * Created by Arnthor Helgi on 11/15/2015.
 */
@Component
public class PlayerCreateFormValidator implements Validator {
    private final PlayerService playerService;

    @Autowired
    public PlayerCreateFormValidator(PlayerService playerService) {
        this.playerService = playerService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(PlayerCreateForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PlayerCreateForm form = (PlayerCreateForm) target;
        validatePasswords(errors, form);
        validateEmail(errors, form);
        validateUsername(errors, form);
    }

    private void validatePasswords(Errors errors, PlayerCreateForm form) {
        if (!form.getPassword().equals(form.getPasswordRepeated())) {
            errors.reject("password.no_match", "Passwords do not match");
        }
    }

    private void validateEmail(Errors errors, PlayerCreateForm form) {
        if (playerService.getUserByEmail(form.getEmail())!=null) {
            errors.reject("email.exists", "User with this email already exists");
        }
    }

    private void validateUsername(Errors errors, PlayerCreateForm form) {
        if (playerService.getUserByUsername(form.getUsername()) != null) {
            errors.reject("username.exists", "User with this username already exists");
        }
    }
}
