package project.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import project.GameState;
import project.persistence.Game;
import project.persistence.Player;
import project.persistence.Slot;
import project.service.CardService;
import project.service.GameService;
import project.service.PlayerService;
import project.service.SlotService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Runar on 26.10.2015.
 */
@Controller
@RequestMapping("/game")
public class GameController {
    GameService gameService;
    CardService cardService;
    PlayerService playerService;
    @Autowired SlotService slotService;

    @Autowired
    public GameController(GameService gameService, CardService cardService, PlayerService playerService) {
        this.gameService = gameService;
        this.cardService = cardService;
        this.playerService = playerService;
    }


    @RequestMapping(value = "", method = RequestMethod.GET)
     public String gameList(Model model) {
        Player player = getCurrentPlayer();

        model.addAttribute("game", new Game());
        List<Game> games = gameService.findAll();
        List<Game> gamesToShow = new ArrayList<>();
        games.stream().filter(g -> g.getSlots().size() != 0).forEach(g -> {
            removeIdlePlayers(g);
            if (g.getSlots().size() != 0) {
                gamesToShow.add(g);
            }
        });
        model.addAttribute("games", gamesToShow);

        Long id = (long) -1;
        for(Game g: games) {
            Long tempId = g.getId();
            if(gameService.hasPlayer(tempId, player)) {
                id = tempId;
            }
        }
        model.addAttribute("id", id);
        String buttonText;
        if (id == -1) {
            buttonText = "Join Game";
        }
        else {
            buttonText = "View Game";
        }
        model.addAttribute("buttonText", buttonText);
        return "games";
    }

    @RequestMapping(value = "/api/gamelist/", method = RequestMethod.GET)
    public @ResponseBody Map<String,Object> gameListAPI() {
        Player player = getCurrentPlayer();
        Map<String, Object> output = new HashMap<>();
        List<Game> games = gameService.findAll();
        ArrayList<Map<String,Object>> gamesToShow = new ArrayList<>();
        for(Game g: games){
            if(g.getSlots().size() != 0){
                Map<String,Object> game = new HashMap<>();
                game.put("id", g.getId());
                game.put("slots", g.getSlots());
                game.put("currentRound", g.getCurrentRound());
                gamesToShow.add(game);
            }
        }
        output.put("games", gamesToShow);

        return output;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String newGame(@ModelAttribute("game") Game game, Model model){
        Player player = getCurrentPlayer();
        if (slotService.findByPlayer(player).size() > 0) {
            return "redirect:/game/";
        }
        game.setDeck(cardService.findAllShuffled());
        gameService.save(game);
        model.addAttribute("game", new Game());
        model.addAttribute("games", gameService.findAll());
        return addPlayer(game.getId());
    }

    @RequestMapping(value = "/api/create", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> newGameApi() {
        HashMap<String, Object> output = new HashMap<>();
        Player player = getCurrentPlayer();
        if (slotService.findByPlayer(player).size() > 0) {
            output.put("error", "Player already in game");
            return output;
        }
        Game game = new Game();
        game.setDeck(cardService.findAllShuffled());
        gameService.save(game);
        output.put("gameId", game.getId());
        return output;
    }

    @RequestMapping(value = "/{id}/", method = RequestMethod.GET)
    public String gameView(@PathVariable long id, Model model) {
        Game game = gameService.findOne(id);
        model.addAttribute("game", game);
        return "game";
    }

    private void updateState(Game game) {
        if (game.getState() == GameState.WRITE_EXPLANATION) {
            // Need at least 2 players to start round
            if (game.getSlots().size() < 2) {
                // Don't start counting down until we have two players
                game.setStateTimeStamp(new Date());
                gameService.save(game);
                return;
            }

            int submissions = 0;
            for (Slot s: game.getSlots()) {
                if (s.getSubmission() != null) {
                    submissions += 1;
                }
            }
            // Wait for at least two submissions before continuing
            if (submissions < 2) {
                return;
            }

            Date d = game.getStateTimeStamp();
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.MINUTE, 1);
            d = cal.getTime();
            // Times up
            if (d.before(new Date())) {
                game.setState(GameState.VOTE_EXPLANATION);
                gameService.save(game);
            } else {
                // Everyone has submitted
                boolean allSubmitted = true;
                for (Slot s : game.getSlots()) {
                    if (s.getSubmission() == null) {
                        allSubmitted = false;
                    }
                }
                if (allSubmitted) {
                    game.setState(GameState.VOTE_EXPLANATION);
                    gameService.save(game);
                }
            }
        } else if (game.getState() == GameState.VOTE_EXPLANATION) {
            Date d = game.getStateTimeStamp();
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.MINUTE, 1);
            d = cal.getTime();
            // Times up
            if (d.before(new Date())) {
                game.setState(GameState.SHOW_RESULTS);
                gameService.save(game);
            } else {
                // Everyone has submitted
                boolean allSubmitted = true;
                for (Slot s : game.getSlots()) {
                    if (s.getVote() == -1 && s.getSubmission() != null) {
                        allSubmitted = false;
                    }
                }
                if (allSubmitted) {
                    game.setState(GameState.SHOW_RESULTS);
                    gameService.save(game);
                }
            }
        } else if (game.getState() == GameState.SHOW_RESULTS) {
            Date d = game.getStateTimeStamp();
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.SECOND, 10);
            d = cal.getTime();
            if (d.before(new Date())) {
                // New round
                game.getDeck().remove(0);
                game.setCurrentRound(game.getCurrentRound() + 1);
                ArrayList<Slot> removeSlots = new ArrayList<>();
                for (Slot s : game.getSlots()) {
                    s.setPoints(s.getPoints()+s.getVotes());
                    s.setVote(-1);
                    s.setVotes(0);
                    if (s.getSubmission() == null) {
                        s.setMissedSubmissions(s.getMissedSubmissions() + 1);
                    } else {
                        s.setMissedSubmissions(0);
                    }
                    s.setSubmission(null);
                    if (s.getMissedSubmissions() >= 2) {
                        removeSlots.add(s);
                    } else {
                        slotService.save(s);
                    }
                }
                for (Slot s : removeSlots) {
                    game.getSlots().remove(s);
                    gameService.save(game);
                    slotService.delete(s);
                }
                game.setState(GameState.WRITE_EXPLANATION);
                gameService.save(game);
            }
        }
    }

    @RequestMapping(value = "/api/{id}/", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> gameJson(@PathVariable long id) {
        Player player = getCurrentPlayer();
        Map<String, Object> output = new HashMap<>();
        Game game = gameService.findOne(id);
        updateState(game);
        output.put("currentRound", game.getCurrentRound());
        output.put("timestamp", game.getStateTimeStamp());
        Optional<Slot> oPlayerSlot = gameService.getSlot(id, player);
        if (!oPlayerSlot.isPresent()) {
            return new HashMap<>();
        }
        Slot playerSlot = oPlayerSlot.get();
        playerSlot.setLastPing(new Date());
        slotService.save(playerSlot);
        if (playerSlot.getSubmission() != null &&
                playerSlot.getSubmission().length() > 0 &&
                game.getState() == GameState.WRITE_EXPLANATION) {
            output.put("state", "WAITING_FOR_OTHER_EXPLANATIONS");
        } else if (game.getState() == GameState.VOTE_EXPLANATION && playerSlot.getVote() > -1) {
            output.put("state", "WAITING_FOR_OTHER_VOTES");
        } else {
            output.put("state", game.getState().name());
        }

        if (game.getDeck().size() == 0) {
            game.setDeck(cardService.findAllShuffled());
            gameService.save(game);
        }
        output.put("word", game.getDeck().get(0).getWord());
        ArrayList<String> explanations = getExplanations(game);
        output.put("explanations", explanations);

        ArrayList<Map<String, Object>> slots = new ArrayList<>();
        List<Slot> slotsArray = game.getSlots();
        Collections.sort(slotsArray);
        for (Slot s : slotsArray) {
            Map<String, Object> slot = new HashMap<>();
            slot.put("id", s.getId());
            slot.put("points", s.getPoints());
            if (game.getState() == GameState.SHOW_RESULTS) {
                slot.put("submission", s.getSubmission());
                slot.put("votes", s.getVotes());
                slot.put("voteId", s.getVote());
                slot.put("votedBy", getVotedByIds(s.getSubmission(), explanations, slotsArray));
            } else {
                slot.put("submission", null);
                slot.put("votedBy", new ArrayList<>());
            }

            slot.put("username", s.getPlayer().getUsername());

            slots.add(slot);
        }
        output.put("slots", slots);
        output.put("playerExplanationId", explanations.indexOf(playerSlot.getSubmission()));
        output.put("correctExplanation", game.getDeck().get(0).getExplanation());
        output.put("correctExplanationVotedBy", getVotedByIds(game.getDeck().get(0).getExplanation(), explanations, slotsArray));
        removeIdlePlayers(game);

        return output;
    }

    private ArrayList<Integer> getVotedByIds(String submission, ArrayList<String> explanations, List<Slot> slotsArray) {
        ArrayList<Integer> arr = new ArrayList<>();
        for (Slot s : slotsArray) {
            if (s.getVote() != -1 && explanations.indexOf(submission) == s.getVote()) {
                arr.add(slotsArray.indexOf(s));
            }
        }
        return arr;
    }

    // Remove players after 30 sec of no ping
    private void removeIdlePlayers(Game game) {
        Date d = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.SECOND, -30);
        d = cal.getTime();
        ArrayList<Slot> removeSlots = new ArrayList<>();
        for (Slot s : game.getSlots()) {
            if (s.getLastPing() == null || d.after(s.getLastPing())) {
                removeSlots.add(s);
            }
        }
        for (Slot s : removeSlots) {
            game.getSlots().remove(s);
            gameService.save(game);
            slotService.delete(s);
        }
    }

    private ArrayList<String> getExplanations(Game game) {
        ArrayList<String> explanations = new ArrayList<>();
        if (game.getState() == GameState.VOTE_EXPLANATION || game.getState() == GameState.SHOW_RESULTS) {
            explanations.addAll(game.getSlots().stream().filter(s -> s.getSubmission() != null).map(Slot::getSubmission).collect(Collectors.toList()));
            explanations.add(game.getDeck().get(0).getExplanation());
            Collections.shuffle(explanations, new Random(game.getId() + game.getCurrentRound() + 1337));
        }
        return explanations;
    }

    @RequestMapping(value = "/api/{id}/explain/", method = RequestMethod.POST)
    public ResponseEntity<String> sendExplanation(@PathVariable long id, @RequestParam("explanation") String explanation) {
        Player player = getCurrentPlayer();
        Optional<Slot> slot = gameService.getSlot(id, player);
        if (slot.isPresent()) {
            slot.get().setSubmission(explanation);
            slotService.save(slot.get());
        } else {
            return new ResponseEntity<>("{\"error\": \"Player not found\"}", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/api/{id}/vote/", method = RequestMethod.POST)
    public ResponseEntity<String> voteExplanation(@PathVariable long id, @RequestParam("explanationId") int explanationId) {
        Player player = getCurrentPlayer();
        Optional<Slot> oSlot = gameService.getSlot(id, player);
        if (!oSlot.isPresent()) {
            return new ResponseEntity<>("{\"error\": \"Player not found\"}", HttpStatus.BAD_REQUEST);
        }
        Slot slot = oSlot.get();
        if (slot.getVote() > -1) {
            return new ResponseEntity<>("{\"error\": \"Already voted\"}", HttpStatus.BAD_REQUEST);
        }
        if (slot.getSubmission() == null) {
            return new ResponseEntity<>("{\"error\": \"Can't vote without submission\"}", HttpStatus.BAD_REQUEST);
        }

        Game game = gameService.findOne(id);
        ArrayList<String> explanations = getExplanations(game);
        boolean ownExplanation = false;
        for (Slot s : game.getSlots()) {
            if (explanations.get(explanationId).equals(s.getSubmission())) {
                // Maybe someone else wrote the same one, then player can vote for that one
                if (s.getPlayer() == player) {
                    ownExplanation = true;
                } else {
                    s.setVotes(s.getVotes()+1);
                    slot.setVote(explanationId);
                    slotService.save(slot);
                    slotService.save(s);
                    return new ResponseEntity<>(HttpStatus.OK);
                }
            }
        }

        if (game.getDeck().get(0).getExplanation().equals(explanations.get(explanationId))) {
            slot.setPoints(slot.getPoints()+1);
            slot.setVote(explanationId);
            slotService.save(slot);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        if (ownExplanation) {
            return new ResponseEntity<>("{\"error\": \"Can't vote for your explanation\"}", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("{\"error\": \"Explanation not found\"}", HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/{id}/join/", method = RequestMethod.POST)
    public String addPlayer(@PathVariable long id) {
        Player player = getCurrentPlayer();
        Game game = gameService.findOne(id);

        // Can't join a game if you are already in one
        if(id == gameService.findGameByPlayer(player)){
            return String.format("redirect:/game/%d/", id);
        }
        if (slotService.findByPlayer(player).size() > 0) {
            return "redirect:/game/";
        }

        List<Slot> slots = game.getSlots();
        if (slots.size() < 6) {
            Slot slot = new Slot(player);
            slot.setLastPing(new Date());
            slot = slotService.save(slot);
            slots.add(slot);
            game.setSlots(slots);
            gameService.save(game);
            return String.format("redirect:/game/%d/", id);
        } else {
            return "redirect:/game/";
        }
    }

    @RequestMapping(value = "/{id}/leave/", method = RequestMethod.POST)
    public String removePlayer(@PathVariable long id) {
        Player player = getCurrentPlayer();

        Game game = gameService.findOne(id);
        List<Slot> slots = game.getSlots();
        Slot removeSlot = null;
        for (Slot s : slots) {
            if (s.getPlayer() == player) {
                removeSlot = s;
            }
        }

        if (removeSlot != null) {
            slots.remove(removeSlot);
            gameService.save(game);
            slotService.delete(removeSlot);
        }

        return "redirect:/game/";
    }

    private Player getCurrentPlayer() {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        return playerService.getUserByEmail(username);
    }
}
