package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import project.persistence.Card;
import project.service.CardService;

/**
 * Created by ArnthorHelgi on 10/25/2015.
 */
@Controller
public class CardController {
    CardService cardService;

    @Autowired
    public CardController(CardService cardService){
        this.cardService = cardService;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/card",method = RequestMethod.GET)
    public String cardViewGet(Model model){
        model.addAttribute("card", new Card());
        model.addAttribute("cards", cardService.findAll());
        return "cards";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/card",method = RequestMethod.POST)
    public String cardViewPost(@ModelAttribute("card") Card card, Model model){
        cardService.save(card);
        model.addAttribute("cards", cardService.findAll());
        model.addAttribute("card", new Card());
        return "cards";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/card/{id}",method = RequestMethod.GET)
    public String cardViewGetEdit(@PathVariable long id, Model model){
        model.addAttribute("card", cardService.findOne(id));
        return "card_edit";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/card/{id}",method = RequestMethod.POST)
    public String cardViewPostEdit(@PathVariable long id, @ModelAttribute("card") Card card, Model model){

        cardService.save(card);
        model.addAttribute("cards", cardService.findAll());
        model.addAttribute("card", new Card());
        return "cards";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/card/{id}/delete",method = RequestMethod.POST)
    public String cardViewPostDelete(@PathVariable long id, @ModelAttribute("card") Card card, Model model){
        cardService.delete(card);
        model.addAttribute("cards", cardService.findAll());
        model.addAttribute("card", new Card());
        return "redirect:/card";
    }
}

