package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import project.persistence.Player;
import project.service.PlayerService;

/**
 * Created by arnor on 11/16/15.
 */
@ControllerAdvice
public class CurrentUserControllerAdvice {
    private PlayerService playerService;

    @Autowired
    public CurrentUserControllerAdvice(PlayerService playerService) {
        this.playerService = playerService;
    }

    @ModelAttribute("currentUser")
    public Player getCurrentUser(Authentication authentication) {
        if (authentication == null) {
            return null;
        }
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return playerService.getUserByEmail(userDetails.getUsername());
    }
}
