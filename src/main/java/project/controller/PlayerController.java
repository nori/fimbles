package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import project.form.PlayerCreateForm;
import project.persistence.Player;
import project.service.PlayerService;
import project.validator.PlayerCreateFormValidator;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Created by Tomas on 26/10/2015.
 */
@Controller
public class PlayerController {
    private final PlayerService playerService;
    private final PlayerCreateFormValidator playerCreateFormValidator;

    @Autowired
    public PlayerController(PlayerService playerService, PlayerCreateFormValidator playerCreateFormValidator) {
        this.playerService = playerService;
        this.playerCreateFormValidator = playerCreateFormValidator;
    }
    @InitBinder("form")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(playerCreateFormValidator);
    }

    @RequestMapping("/api/me")
    public @ResponseBody Map<String, Object> getUserInfo() {
        // TODO: decide what attributes should be here
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        Player player = playerService.getUserByEmail(username);
        Map<String, Object> output = new HashMap<>();
        output.put("username", player.getUsername());
        return output;
    }

    @RequestMapping("/player")
    public ModelAndView getUsersPage() {
        return new ModelAndView("players", "players", playerService.getAllUsers());
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/player/{id}", method = RequestMethod.GET)
    public ModelAndView getUserPageGet(@PathVariable Long id) {
        Player p = playerService.getUserById(id);
        if (p == null) {
            throw new NoSuchElementException(String.format("User=%s not found", id));
        } else {
            return new ModelAndView("user", "user", p);

        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/player/{id}", method = RequestMethod.POST)
    public String getUserPagePost(@PathVariable Long id, @ModelAttribute("user.role") Player player, Model model) {
        Player p = playerService.getUserById(id);
        if (p == null) {
            throw new NoSuchElementException(String.format("User=%s not found", id));
        } else {
            p.setRole(player.getRole());
            playerService.save(p);
            return "redirect:/player";

        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/player/{id}/delete", method = RequestMethod.POST)
    public String getUserPageDelete(@PathVariable Long id) {
        Player p = playerService.getUserById(id);
        playerService.delete(p);
        if (p == null) {
            throw new NoSuchElementException(String.format("User=%s not found", id));
        } else {
            return "redirect:/player";

        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/player/create", method = RequestMethod.GET)
    public ModelAndView getUserCreatePage() {
        return new ModelAndView("user_create", "form", new PlayerCreateForm());
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/player/create", method = RequestMethod.POST)
    public String handleUserCreateForm(@Valid @ModelAttribute("form") PlayerCreateForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "user_create";
        }
        try {
            playerService.create(form);
        } catch (DataIntegrityViolationException e) {
            bindingResult.reject("email.exists", "Email already exists");
            return "user_create";
        }
        return "redirect:/player";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public ModelAndView getSignupPage() {
        return new ModelAndView("signup", "form", new PlayerCreateForm());
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String handleSignupForm(@Valid @ModelAttribute("form") PlayerCreateForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "signup";
        }
        try {
            playerService.create(form);
        } catch (DataIntegrityViolationException e) {
            bindingResult.reject("email.exists", "Email already exists");
            return "signup";
        }
        return "redirect:/";
    }
}
