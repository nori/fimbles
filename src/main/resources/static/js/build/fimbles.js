var RoundCounter = React.createClass({
    displayName: "RoundCounter",

    render: function () {
        return React.createElement(
            "h1",
            null,
            "Round ",
            this.props.round
        );
    }
});

var Clock = React.createClass({
    displayName: "Clock",

    getInitialState: function () {
        return { time: Date.now() };
    },
    updateTime: function () {
        this.setState({ time: Date.now() });
    },
    componentDidMount: function () {
        setInterval(this.updateTime, 100);
    },
    render: function () {
        var now = Date.now();
        var lastStateTimeStamp = this.props.timeStamp;
        var state = this.props.state;
        var max = 60;
        var offset = max * 1000;
        if (state === "VOTE_EXPLANATION") {
            max = 60;
            offset = max * 1000;
        } else if (state === "WRITE_EXPLANATION") {
            max = 60;
            offset = max * 1000;
        } else if (state === "SHOW_RESULTS") {
            max = 10;
            offset = max * 1000;
        }
        var timeLeft = (lastStateTimeStamp + offset - now) / 1000;
        var percent = Math.max(Math.ceil(timeLeft), 0);
        return React.createElement(
            "div",
            { className: "timeLeft" },
            React.createElement(
                "h4",
                null,
                "Time left: ",
                percent,
                " seconds"
            ),
            React.createElement("progress", { max: max, value: timeLeft })
        );
    }
});

var Word = React.createClass({
    displayName: "Word",

    render: function () {
        return React.createElement(
            "h1",
            null,
            "Current word: ",
            this.props.word
        );
    }
});

var ExplanationBox = React.createClass({
    displayName: "ExplanationBox",

    getInitialState: function () {
        if (this.props.state === "WAITING_FOR_OTHER_EXPLANATIONS") {
            return { hasSent: true, isSending: true, text: '' };
        } else {
            return { hasSent: false, isSending: false, text: '' };
        }
    },
    componentWillReceiveProps: function (newProps) {
        if (newProps.state === "WAITING_FOR_OTHER_EXPLANATIONS") {
            this.setState({ hasSent: true, isSending: true });
        } else if (this.props.state !== "WRITE_EXPLANATION") {
            this.setState({ hasSent: false, isSending: false });
        }
    },
    submitExplanation: function () {
        console.log(this.props);
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        var headers = {};
        headers[header] = token;
        this.setState({ isSending: true });
        $.ajax({
            type: "POST",
            //headers: headers,
            url: '/game/api/' + this.props.gameId + '/explain/',
            data: { explanation: this.state.text },
            success: (function (data, textStatus, jqXHR) {
                this.setState({ isSending: false, hasSent: true });
            }).bind(this),
            error: (function (jqXHR, textStatus, errorThrown) {
                // TODO: display error
                console.log(errorThrown);
                this.setState({ isSending: false });
            }).bind(this)
        });
    },
    handleChange: function (event) {
        this.setState({ text: event.target.value });
    },
    render: function () {
        if (this.props.state === "WAITING_FOR_OTHER_EXPLANATIONS" || this.props.state === "WRITE_EXPLANATION" && this.state.hasSent) {
            return React.createElement(
                "p",
                null,
                "Explanation sent, waiting for other players..."
            );
        }
        if (this.state.isSending) {
            return React.createElement(
                "p",
                null,
                "Sending explanation.."
            );
        }
        var visible = this.props.state === "WAITING_FOR_OTHER_EXPLANATIONS" || this.props.state === "WRITE_EXPLANATION";
        var classes = "explanation-box " + (visible ? '' : 'hidden');
        return React.createElement(
            "div",
            { className: classes },
            React.createElement("textarea", { className: "form-control", placeholder: this.props.word + " is ...", onChange: this.handleChange }),
            React.createElement("br", null),
            React.createElement(
                "button",
                { className: "btn btn-primary", onClick: this.submitExplanation },
                "Submit"
            )
        );
    }
});

var SlotInfo = React.createClass({
    displayName: "SlotInfo",

    render: function () {
        return React.createElement(
            "div",
            { className: "playerSlot " + this.props.cssClass },
            React.createElement(
                "h3",
                null,
                this.props.slot.username,
                " (",
                this.props.slot.points,
                " points)"
            )
        );
    }
});

var ResultInfo = React.createClass({
    displayName: "ResultInfo",

    render: function () {
        var visible = "hidden";
        if (this.props.state === "SHOW_RESULTS" && this.props.slot.submission !== null) {
            visible = " ";
        }
        return React.createElement(
            "div",
            { className: "playerSlot " + " col-xs-6 " + visible },
            React.createElement(
                "div",
                { className: "list-group-item " + this.props.cssClass + "Result" },
                this.props.slot.votedBy.map(function (id) {
                    return React.createElement("div", { className: "vote-box player" + id + "Result", key: id });
                }),
                React.createElement(
                    "p",
                    null,
                    this.props.slot.submission
                ),
                React.createElement(
                    "em",
                    null,
                    this.props.slot.username
                )
            )
        );
    }
});

var ExplanationList = React.createClass({
    displayName: "ExplanationList",

    render: function () {
        var self = this;
        var clickable = true;
        if (this.props.state === "WAITING_FOR_OTHER_VOTES") {
            clickable = false;
        }
        var visible = this.props.state === "WAITING_FOR_OTHER_VOTES" || this.props.state === "VOTE_EXPLANATION";
        var classes = "row explanation-list " + (visible ? '' : 'hidden');
        return React.createElement(
            "div",
            { className: classes },
            this.props.list.map(function (explanation, index) {
                return React.createElement(ExplanationItem, { isPlayers: self.props.playerId == index, clickable: clickable, key: index, explanation: explanation, explanationId: index, state: self.props.state, gameId: self.props.gameId });
            })
        );
    }
});

var ExplanationItem = React.createClass({
    displayName: "ExplanationItem",

    getInitialState: function () {
        return { hasSent: false, isSending: false, error: undefined };
    },
    vote: function () {
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        var headers = {};
        headers[header] = token;
        this.setState({ isSending: true, error: undefined });
        $.ajax({
            type: "POST",
            //headers: headers,
            url: '/game/api/' + this.props.gameId + '/vote/',
            data: { explanationId: this.props.explanationId },
            success: (function (data, textStatus, jqXHR) {
                this.setState({ isSending: false, hasSent: true });
            }).bind(this),
            error: (function (jqXHR, textStatus, errorThrown) {
                var message = JSON.parse(jqXHR.responseText).error;
                this.setState({ isSending: false, error: { message: message, date: new Date() } });
            }).bind(this)
        });
    },
    render: function () {
        var classes = "list-group-item list-group-item";
        if (this.props.isPlayers) {
            classes += "-warning";
        } else if (this.state.isSending) {
            classes += "-primary";
        } else if (this.state.hasSent) {
            classes += "-success";
        } else if (!this.props.clickable) {
            classes += "-info";
        }
        var error = undefined;
        if (this.state.error !== undefined) {
            var t = new Date();
            t.setSeconds(t.getSeconds() - 3);
            if (this.state.error.date.getTime() > t.getTime()) {
                error = React.createElement(
                    "p",
                    null,
                    this.state.error.message
                );
            }
        }
        return React.createElement(
            "div",
            { className: "col-xs-6" },
            React.createElement(
                "a",
                { href: "#", className: classes, onClick: this.vote },
                this.props.explanation
            ),
            error
        );
    }
});

var Game = React.createClass({
    displayName: "Game",

    getInitialState: function () {
        return { game: {} };
    },
    loadGame: function () {
        $.get('/game/api/' + this.props.gameId + '/', (function (response) {
            this.setState({ game: response });
        }).bind(this));
    },
    componentDidMount: function () {
        this.loadGame();
        setInterval(this.loadGame, this.props.pollInterval);
    },
    render: function () {
        if (this.state.game.slots === undefined) {
            return React.createElement(
                "h1",
                null,
                "Loading"
            );
        }
        var state = this.state.game.state;
        var game = this.state.game;
        var correctSlot = {
            id: 0,
            submission: game.correctExplanation,
            votedBy: game.correctExplanationVotedBy,
            username: 'Correct explanation'
        };
        return React.createElement(
            "div",
            { className: "game row" },
            React.createElement(
                "div",
                { className: "col-xs-3" },
                React.createElement(RoundCounter, { round: this.state.game.currentRound }),
                React.createElement(
                    "div",
                    { className: "userList" },
                    this.state.game.slots.map(function (slot, index) {
                        return React.createElement(SlotInfo, { slot: slot, key: slot.id, state: state, cssClass: 'player' + index });
                    })
                )
            ),
            React.createElement(
                "div",
                { className: "col-xs-7" },
                React.createElement(Word, { word: this.state.game.word }),
                React.createElement(ExplanationBox, { state: state, word: this.state.game.word, gameId: this.props.gameId }),
                React.createElement(ExplanationList, { state: state, list: this.state.game.explanations, gameId: this.props.gameId, playerId: this.state.game.playerExplanationId }),
                React.createElement(
                    "div",
                    { className: "resultList row" },
                    this.state.game.slots.map(function (slot, index) {
                        return React.createElement(ResultInfo, { slot: slot, key: slot.id, state: state, cssClass: 'player' + index });
                    }),
                    React.createElement(ResultInfo, { slot: correctSlot, key: correctSlot.id, state: state, cssClass: 'player' + 6 })
                ),
                React.createElement(Clock, { timeStamp: this.state.game.timestamp, state: this.state.game.state })
            )
        );
    }
});

ReactDOM.render(React.createElement(Game, { gameId: document.getElementById('react').dataset.gameId, pollInterval: 1000 }), document.getElementById('react'));