<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ tag description="Simple Wrapper Tag" pageEncoding="UTF-8"%>
<%@ attribute name="title" required="true"%>
<%@ attribute name="header" fragment="true" %>
<%@ attribute name="body" fragment="true" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
    <head>
        <title>${title}</title>

        <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrap-3.3.5-dist/css/bootstrap.css"/>"/>
        <jsp:invoke fragment="header"/>
    </head>
    <body>
      <nav class="navbar navbar-default">
          <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                  <a class="navbar-brand" href="<c:url value="/"/>">Fimbles</a>
                  <a class="navbar-brand" href="<c:url value="/fimbles.apk"/>" download>Download app</a>
              </div>

              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                      <li><a href="<c:url value="/game"/>">Games</a></li>
                      <sec:authorize access="hasRole('ADMIN')">
                          <li><a href="<c:url value="/card"/>">Cards</a></li>
                          <li><a href="<c:url value="/player/"/>">Players</a></li>
                      </sec:authorize>
                  </ul>
                  <ul class="nav navbar-nav navbar-right">
                      <sec:authorize access="isAnonymous()">
                          <li><a href="<c:url value="/login"/>">Login</a></li>
                          <li><a href="<c:url value="/signup"/>">Signup</a></li>
                      </sec:authorize>
                      <sec:authorize access="isAuthenticated()">
                          <li class="navbar-text">${currentUser.getUsername()}</li>
                          <li>
                              <form:form class="form-inline" action="/logout" method="post">
                                  <a href="javascript:;" onclick="parentNode.submit();" class="navbar-text navbar-link">Logout</a>
                              </form:form>
                          </li>
                      </sec:authorize>
                  </ul>
              </div>
          </div>
      </nav>
      <jsp:invoke fragment="body"/>
    </body>
</html>