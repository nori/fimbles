<!DOCTYPE html>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="../css/login.css" rel="stylesheet" type="text/css">
    <title>Sign up</title>
</head>
<t:wrapper title="Players">

    <jsp:attribute name="body">
        <div tiles:fragment="content" class ="container">
            <div class="main">
                <h1>Sign up!</h1>

                <form role="form" name="form" action="" method="post">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <label for="email">Email address</label>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">@</span>
                        <input type="email" name="email" id="email" value="${form.email}" required autofocus class="form-control" />
                    </div>
                    <label for="username">Username</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input type="text" name="username" id="username" value="${form.username}" required class="form-control"/>
                    </div>
                    <label for="password">Password</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input type="password" name="password" id="password" required class="form-control"/>
                    </div>
                    <label for="passwordRepeated">Repeat</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input type="password" name="passwordRepeated" id="passwordRepeated" required class="form-control"/>
                    </div>
                    <div class="col-xs-6 col-md-6 pull-right">
                        <button type="submit" class="btn btn-large btn-success pull-right">Save</button>
                    </div>
                </form>
            </div>
        </div>
   </jsp:attribute>
</t:wrapper>