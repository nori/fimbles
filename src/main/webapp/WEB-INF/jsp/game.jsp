<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:wrapper title="Games">
    <jsp:attribute name="header">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/game.css"/>"/>
        <meta name="_csrf" content="${_csrf.token}"/>
	    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    </jsp:attribute>
    <jsp:attribute name="body">
        <form:form class="form-inline" action="/game/${game.id}/leave/" method="post">
            <a href="javascript:;" class="btn btn-warning pull-right" onclick="parentNode.submit();">Leave game</a>
        </form:form>
        <div id="react" data-game-id="${game.id}"></div>
        <script src="/js/lib/jquery-1.11.3.min.js"></script>
        <script src="/js/lib/react.js"></script>
        <script src="/js/lib/react-dom.js"></script>
        <script src="/js/build/fimbles.js"></script>
    </jsp:attribute>
</t:wrapper>