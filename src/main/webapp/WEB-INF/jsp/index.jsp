<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:wrapper title="Fimbles">
  <jsp:attribute name="body">
   <div class="container">

        <h1>Fimbles!</h1>
        <p>
          Fimbles is a Web Application that allows you to play the game Fimbles where you describe rare English words. Fimbles got it's idea from the Icelandic board game Fimbulfamb and is a digitalized version of that same game.
        </p>
        <p>How it works:</p>
        <ul>
          <li>Player gets a card with a word on it.</li>
          <li>Player tries to guess what the word means. He submits his description of the word.</li>
          <li>When all players has submitted there descriptions of the word all the descriptions appear on the screen plus the right description from the admin.</li>
          <li>Every player now needs to vote what description they think is the right one. Player gets a point if they vote the right description or other players vote their description.</li>
          <li> NOTE: Player can't vote his own description.</li>
         </ul>
    </div>
  </jsp:attribute>
</t:wrapper>