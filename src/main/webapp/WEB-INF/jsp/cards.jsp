<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>



<t:wrapper title="Cards">
    <jsp:attribute name="header">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/cards.css"/>"/>
    </jsp:attribute>
    <jsp:attribute name="body">

    <sf:form method="POST" commandName="card" action="/card">

        <table>
            <tr>
                <td> Word:</td>
                <td><sf:input path="word" type="text" placeholder="Word"/></td>
            </tr>
            <tr>
                <td>Explanations:</td>
                <td><sf:textarea path="explanation" type="text" placeholder="Explanation text here"/></td>
            </tr>
        </table>
        <input type="hidden"
                    		name="${_csrf.parameterName}"
                    		value="${_csrf.token}"/>
        <input class="btn btn-success" type="submit" VALUE="Submit Card!"/>

    </sf:form>

    <c:choose>
        <c:when test="${not empty cards}">
            <table class="table table-striped explanation">
                <thead>
                    <th>Word</th>
                    <th>Explanation</th>
                </thead>
                <tbody>
                <c:forEach var="card" items="${cards}">
                    <tr>
                        <td>${card.word}</td>
                        <td>${card.explanation}</td>
                        <td><a class="btn btn-primary" href="/card/${card.id}">Edit</a></td>
                        <td>
                    </tr>
                </c:forEach>
                <tbody>
            </table>
        </c:when>

        <c:otherwise>
            <h3>No notes!</h3>
        </c:otherwise>
    </c:choose>

    </jsp:attribute>
</t:wrapper>