<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>



<t:wrapper title="Cards">
    <jsp:attribute name="header">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/cards.css"/>"/>
    </jsp:attribute>
    <jsp:attribute name="body">

    <sf:form method="POST" commandName="card" action="/card/${card.id}">

        <table>
            <tr>
                <td> Word:</td>
                <td><sf:input path="word" type="text" placeholder="Word"/></td>
            </tr>
            <tr>
                <td>Explanations:</td>
                <td><sf:textarea path="explanation" type="text" placeholder="Explanation text here"/></td>
            </tr>
        </table>
        <input type="hidden"
                    		name="${_csrf.parameterName}"
                    		value="${_csrf.token}"/>
        <input class="btn btn-success" type="submit" VALUE="Submit Card!"/>

    </sf:form>
    <br>
    <sf:form method="POST" commandName="card" action="/card/${card.id}/delete">
        <input type="hidden"
                            name="${_csrf.parameterName}"
                            value="${_csrf.token}"/>
        <input class="btn btn-danger" type="submit" VALUE="Delete Card!"/>
    </sf:form>


    </jsp:attribute>
</t:wrapper>