<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<html lang="en">

    <head>
        <title>User Page</title>
    </head>
    <t:wrapper title="Players">

        <jsp:attribute name="body">

            <h1>User Page</h1>

            <form role="form" name="form" action="/player/${user.id}" method="POST">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <div>
                    <label for="email">Email address</label>
                    <input type="email" name="email" id="email" value="${user.email}" readonly/>
                </div>
                <div>
                    <label for="username">Username</label>
                    <input type="text" name="username" id="username" value="${user.username}" readonly/>
                </div>
                <sec:authorize access="hasRole('ADMIN')">
                    <div>
                        <label for="role">Role</label>
                        <select name="role" id="role" required>
                            <option ${user.role.name() == 'USER' ? 'selected' : ''}>USER</option>
                            <option ${user.role.name() == 'ADMIN' ? 'selected' : ''}>ADMIN</option>
                        </select>
                    </div>
                </sec:authorize>
                <button class="btn btn-success" type="submit">Save</button>
            </form>

            <form role="form" name="form" action="/player/${user.id}/delete" method="POST">
                <input type="hidden"
                                    name="${_csrf.parameterName}"
                                    value="${_csrf.token}"/>
                <input class="btn btn-danger" type="submit" VALUE="Delete player!"/>
            </form>


    </jsp:attribute>
    </t:wrapper>