<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html xmlns:th="http://www.thymeleaf.org" xmlns:tiles="http://www.thymeleaf.org">
<head>
    <title tiles:fragment="title">Please Login</title>

    <link href="../css/login.css" rel="stylesheet" type="text/css">
</head>
<t:wrapper title="Players">

    <jsp:attribute name="body">
        <div class="container">
            <div tiles:fragment="content" class ="main">
                <h1>Login to Your Account</h1><br>
                <form role="form" action="/login" method="post">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <label for="email">Email address</label>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">@</span>
                        <input type="email" name="email" id="email" required autofocus placeholder="Email" class="form-control">
                    </div>
                    <label for="password">Password</label>
                    <div class="input-group">

                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input type="password" name="password" id="password" required placeholder="Password" class="form-control">
                    </div>
                    <div>
                        <label for="remember-me">Remember me</label>
                        <input type="checkbox" name="remember-me" id="remember-me">
                    </div>
                    <div class="col-xs-6 col-md-6 pull-right">
                        <button type="submit" class="btn btn-large btn-success pull-right">Sign in</button>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </div>
                </form>
            </div>
        </div>
   </jsp:attribute>
</t:wrapper>