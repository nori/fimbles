<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:wrapper title="Games">
    <jsp:attribute name="header">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/games.css"/>"/>
    </jsp:attribute>
    <jsp:attribute name="body">
    <div class="row">
    <c:if test="${id==-1}">
        <form:form method="POST" commandName="game" action="/game" class="col-xs-8 col-xs-offset-4">
            <input class="btn btn-lg btn-primary" type="submit" VALUE="New Game!"/>
        </form:form>
    </c:if>

    <c:choose>
        <c:when test="${not empty games}">
        <div class="col-xs-12">
            <table class="explanation table">
                <thead>
                    <tr>
                        <th>Game</th>
                        <th>Round</th>
                        <th>Playing</th>
                        <th><th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="game" items="${games}">
                        <tr class="${game.id==id ? 'success' : game.slots.size() == 6 ? 'danger' : ''}">
                            <td>Game ${game.id}</td>
                            <td>${game.currentRound}</td>
                            <td>${game.slots.size()} / 6</td>
                            <c:choose>
                                <c:when test="${id==-1}">
                                    <td>
                                        <form:form class="form-inline" action="/game/${game.id}/join/" method="post">
                                            <a class="btn btn-success" href="javascript:;" onclick="parentNode.submit();">${buttonText}</a>
                                        </form:form>
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td>
                                        <c:if test="${game.id==id}">
                                            <form:form class="form-inline" action="/game/${game.id}/join/" method="post">
                                                <a class="btn btn-success" href="javascript:;" onclick="parentNode.submit();">${buttonText}</a>
                                            </form:form>
                                        </c:if>
                                    </td>
                                </c:otherwise>
                            </c:choose>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        </c:when>

        <c:otherwise>
            <h3 class="col-xs-12">No games!</h3>
        </c:otherwise>
    </c:choose>

    </div>

    </jsp:attribute>
</t:wrapper>