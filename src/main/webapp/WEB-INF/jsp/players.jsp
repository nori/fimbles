<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>List of Users</title>
</head>
<t:wrapper title="Players">

    <jsp:attribute name="body">
        <%--<body>
        <nav role="navigation">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/player/create">Create a new user</a></li>
            </ul>
        </nav>
        --%>
        <h1>List of Users</h1>

        <table class="table">
            <thead>
            <tr>
                <th>Username</th>
                <th>E-mail</th>
                <th>Role</th>
            </tr>
            </thead>
            <tbody>
                <c:forEach var="player" items="${players}">
                    <tr>
                        <td><a href="/player/${player.id}">${player.username}</a></td>
                        <td>${player.email}</td>
                        <td>${player.role}</td>
                    </tr>
                </c:forEach>
            </#list>
            </tbody>
        </table>

    <%--</body>
    </html>--%>
    </jsp:attribute>
</t:wrapper>